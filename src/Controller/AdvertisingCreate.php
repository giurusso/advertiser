<?php


namespace App\Controller;


use App\DTO\DTOAdvertising;
use App\Entity\Advertisement;
use App\Form\AdvertisingType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdvertisingCreate extends AbstractController
{

    /**
     * @Route("/advertisingCreate", name="advertising_create")
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param TranslatorInterface $translator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(EntityManagerInterface $entityManager, Request $request, FormFactoryInterface $formFactory, TranslatorInterface $translator )
    {
        $form = $formFactory->create(AdvertisingType::class, new DTOAdvertising());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $title = $data->getTitle();

            $advertising = new Advertisement($title);
            $entityManager->persist($advertising);
            $entityManager->flush();

            return $this->redirectToRoute('all_advertisements');
        }

        return $this->render('AdvertisingCreate.twig', [
            'formular' => $form->createView(),
        ]);
    }
}
