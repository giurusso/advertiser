<?php


namespace App\Controller;


use App\Repository\AdvertisementRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdvertisingList extends AbstractController
{

    /**
     * @param EntityManagerInterface $entityManager
     * @param AdvertisementRepository $advertisementRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/alladvertisements", name="all_advertisements")
     */
    public function __invoke(EntityManagerInterface $entityManager, AdvertisementRepository $advertisementRepository, Request $request)
    {
        $allAdvertisements = $advertisementRepository->findAll();

        return $this->render('AdvertisingList.twig', [
            'AllAdvertisements' => $allAdvertisements,
        ]);
    }
}
