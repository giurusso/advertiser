<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Home extends AbstractController
{

    /**
     * @Route("/home", name="advertiser_home")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke()
    {

        return $this->render('/Home.twig', [

        ]);
    }
}

