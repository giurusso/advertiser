<?php


namespace App\Form;


use App\DTO\DTOAdvertising;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvertisingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', TextareaType::class,[
                'attr' => ['class' => 'form-control', 'id' => 'exampleTextarea', 'rows' => '3', 'cols' => '60', 'placeholder' => 'Sujet', 'maxlength' => 50],
                'label' => false,
                'required' => true,
                'empty_data' => ''
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault("data_class", DTOAdvertising::class);
    }
}
