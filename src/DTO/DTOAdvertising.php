<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class DTOAdvertising
{

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 2,
     *     max = 50,
     *     minMessage = "L'intitulé du sujet doit être composé d'au moins deux caractères",
     *     maxMessage = "L'intitulé du sujet ne doit pas dépasser cinquante caractères",
     * )
     */
    private $title;


    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }


    /**
     * @param string $text
     */
    public function setTitle(string $text) : void
    {
        $this->title = $text;
    }
}
