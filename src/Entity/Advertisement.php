<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Advertisement
 * @ORM\Entity(repositoryClass="App\Repository\AdvertisementRepository")
 * @ORM\Table(name="advertisement")
 */
class Advertisement
{

    //================================
    // Properties
    //================================

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId() : int
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="string", nullable=false, length=50)
     * @Assert\Length(
     *      min = 8,
     *      max = 50,
     *      minMessage = "Le champ du sujet doit contenir au moins 2 caractères",
     *      maxMessage = "Le champ du sujet ne peut pas dépasser 50 caractères",
     * )
     */
    private $title;


    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function __construct(string $title)
    {
        if ($title == null){
            throw new InvalidArgumentException("Title cannot be null");
        }

        $this->title = $title;
    }
}
